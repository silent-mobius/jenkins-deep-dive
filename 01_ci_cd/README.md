
---

# CI/CD


---

# What is CI / CD?

## CI stands for Continuous Integration

 -  CI is a practice
 -  Code is integrated with other developers
 -  Most commonly the way to integrate code is to check if the build step is still working
 -  A common practice is to also check if unit tests still work
 -  Coding guidelines should also be considered (can be mostly automated)
 -  Work is integrated all the time, multiple times per day
 -  Most of the time the goal of the CI pipeline is to build a package that can be deployed
---

# What is CI / CD?

##  CD can stand for Continuous Delivery

 -  CD is done after CI
 -  One cannot do CD without first doing CI
 -  The goal of CD is to take the package created by the CI pipeline and to test it further
    -  Making sure it can be installed (by actually installing the package on a system similar to production)
    -  Run additional tests to check if the package integrates with other systems
 -  After a manual check/decision, the package can be installed on a production system as well

---

# What is CI / CD?

##  CD can also stand for Continuous Deployment

 -  CD goes a step further and automatically installs every package to production
 the package must first go through all previous stages successfully
 -  No manual intervention is required

---

# What is CI / CD?

##  The advantages of CI

 -  Detecting errors early in the development process
 -  Reduces integration errors
 -  Allow developers to work faster

---

# What is CI / CD?

##  The advantages of CD

 -  Ensure that every change is releasable
 -  Reduces the risk of a new deployment
 -  Delivers value much faster (changes are released more often


---

# Key terminology

## Project or a Job

- A user-configured description of the work that CI/CD will manage
- Job and Project might be used interchangeably

---

# Key terminology

## Build

- `build`:  set of instructions  which CI go though in a job
- `build step`: specific command inside CI `build`
  - Jobs can contain as many build steps as needed
- `build trigger`: event that starts the `build`
  - manual: creating a job via clicking on a button in UI
  - automatic: via RestAPI request or with schedular


---

# Pop Quiz:

Which below is NOT a CI/CD System?
- GitLab
- Team-city
- Cruise-control
- Launchpad