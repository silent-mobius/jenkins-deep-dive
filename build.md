# Jenkins Deep Dive



.footer: Created By Alex M. Schapelle, VAioLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is CI/CD ?
- What is Jenkins ?
- How Jenkins works ?
- What are Jenkins pipelines ?
- How to manage docker in various scenarious ?


### Who Is This course for ?

- Junior/senior developers who wish to automate their development tasks.
- For junior/senior developers/devops/sysops who wish to learn CI/CD Basics with Jenkins as main tool.
- Experienced developers/devops who need refresher.


---

# Course Topics

- Intro
- CI/CD Basics
- Jenkins Environment Setup
- Jenkins Plugins
- Jenkins Pipelines

---

# About Me
<img src="misc/.img/me.jpg" alt="me" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---

# History

In software engineering, CI/CD or CICD is the combined practices of continuous integration (CI) and (more often) continuous delivery or (less often) continuous deployment (CD).

CI/CD bridges the gaps between development and operation activities and teams by enforcing automation in building, testing and deployment of applications. CI/CD services compile the incremental code changes made by developers, then link and package them into software deliverables. 

Automated tests verify the software functionality, and automated deployment services deliver them to end users. 

---

# History (cont.)

The aim is to increase early defect discovery, increase productivity, and provide faster release cycles. The process contrasts with traditional methods where a collection of software updates were integrated into one large batch before deploying the newer version. 

Modern-day DevOps practices involve continuous development, continuous testing, continuous integration, continuous deployment and continuous monitoring of software applications throughout its development life cycle. The CI/CD practice, or CI/CD pipeline, forms the backbone of modern day DevOps operations. 

---
# History (cont.)

## Jenkins

Kohsuke Kawaguchi worked at Sun Microsystems on numerous projects for the Java, XML and Solaris ecosystems, notably as the primary developer for Hudson and for Multi Schema Validator. Hudson was created in summer of 2004 and first released in February 2005.

During November 2010, after the acquisition of Sun Microsystems by Oracle, an issue arose in the Hudson community with respect to the infrastructure used, which grew to encompass questions over the stewardship and control by Oracle. Negotiations between the principal project contributors and Oracle took place, and although there were many areas of agreement a key sticking point was the trademarked name "Hudson," after Oracle claimed the right to the name and applied for a trademark in December 2010. As a result, on January 11, 2011, a call for votes was made to change the project name from "Hudson" to "Jenkins." The proposal was overwhelmingly approved by community vote on January 29, 2011, creating the Jenkins project.

---
# History (cont.)

## Jenkins

On February 1, 2011, Oracle said that they intended to continue development of Hudson, and considered Jenkins a fork rather than a rename. Jenkins and Hudson therefore continued as two independent projects,each claiming the other is the fork. As of June 2019, the Jenkins organization on GitHub had 667 project members and around 2,200 public repositories, compared with Hudson's 28 project members and 20 public repositories with the last update in 2016.

On April 20, 2016 version 2 was released with the Pipeline plugin enabled by default. The plugin allows for writing build instructions using a domain specific language based on Apache Groovy.

---

# Why Jenkins

- Easy of use
- 1200+ plugins
- Reporting
- Distributed builds

---

# Jenkins in nutshell

- Originally written by Kohsuke Kawaguchi (as Hudson)
- Open-source CI Server
- Released in 2008
- Renamed to Jenkins in 2011
- 1800+ plugins available on official site
- http://jenkins.io


---

# CI/CD


---

# What is CI / CD?

## CI stands for Continuous Integration

 -  CI is a practice
 -  Code is integrated with other developers
 -  Most commonly the way to integrate code is to check if the build step is still working
 -  A common practice is to also check if unit tests still work
 -  Coding guidelines should also be considered (can be mostly automated)
 -  Work is integrated all the time, multiple times per day
 -  Most of the time the goal of the CI pipeline is to build a package that can be deployed
---

# What is CI / CD?

##  CD can stand for Continuous Delivery

 -  CD is done after CI
 -  One cannot do CD without first doing CI
 -  The goal of CD is to take the package created by the CI pipeline and to test it further
    -  Making sure it can be installed (by actually installing the package on a system similar to production)
    -  Run additional tests to check if the package integrates with other systems
 -  After a manual check/decision, the package can be installed on a production system as well

---

# What is CI / CD?

##  CD can also stand for Continuous Deployment

 -  CD goes a step further and automatically installs every package to production
 the package must first go through all previous stages successfully
 -  No manual intervention is required

---

# What is CI / CD?

##  The advantages of CI

 -  Detecting errors early in the development process
 -  Reduces integration errors
 -  Allow developers to work faster

---

# What is CI / CD?

##  The advantages of CD

 -  Ensure that every change is releasable
 -  Reduces the risk of a new deployment
 -  Delivers value much faster (changes are released more often


---

# Key terminology

## Project or a Job

- A user-configured description of the work that CI/CD will manage
- Job and Project might be used interchangeably

---

# Key terminology

## Build

- `build`:  set of instructions  which CI go though in a job
- `build step`: specific command inside CI `build`
  - Jobs can contain as many build steps as needed
- `build trigger`: event that starts the `build`
  - manual: creating a job via clicking on a button in UI
  - automatic: via RestAPI request or with schedular


---

# Pop Quiz:

Which below is NOT a CI/CD System?
- GitLab
- Team-city
- Cruise-control
- Launchpad
---

# Jenkins Setup


---

# Setup with Vagrant

In misc folder of this project we can find folder named vagrant, and in it file named `Vagrantfile` which essentially has all the configurations in it  for us to get ready jenkins server. Yet , it usually requires some dependencies:
- Virtualization capable hardware:
  - CPU
  - RAM
  - Storage
- Hypervisor, either [Virtualbox](https://www.virtualbox.org/wiki/Downloads) or [KVM](https://www.linux-kvm.org/page/Main_Page)(only on Linux/Unix) should work out of the box.
- [Vagrant](https://www.vagrantup.com/downloads) itself also is required.

---

# Setup with Vagrant (cont.)

After installing all pre-requisite software, you may change directory to `~/jenkins-deep-dive/misc/set_up/vagrant`, and start the environment as follows:

```sh
bash:~/jenkins-deep-dive/misc/vagrant $ vagrant up 
```
If all goes well, you should have working jenkins environment.

> `[!]` Note: Vagrant will start 3 capable VMs, meaning it will have some-what of a stress on your machine. In case you do not have enough resources such as CPU or ram, it is suggested not to use `Vagrant setup`

---

# Setup with Vagrant (cont.)

In case it fails, Please ask instructor/developer/who ever provided you with this project to help you with subsequent issue.

In other case, in which your host can not comply with CPU overload, RAM stress or Storage in-sufficiency, you are welcome to try and to use `docker-compose setup `


---

# Setup with docker-compose

In cases where using `vagrant` is not an option, we can provide installation of docker and docker-compose.

Lets start with docker installation
```sh
bash:~/$ curl -L get.docker.com | sudo bash 
```
or user `wget` in case `curl` command is

```sh
bash:~/$ wget -O- get.docker.com |sudo bash
```

> `[!]` Note: you need to add your user to docker group and restart your session to gain correct access and use permissions.

---

# Setup with docker-compose (cont.)

Docker Compose relies on *Docker Engine* for any meaningful work. Once it is installed, set and configured (mostly done while installation), we can download  and set `docker-compose`.

```sh
bash~$ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)"\
                 -o /usr/local/bin/docker-compose
bash~$ sudo chmod +x /usr/local/bin/docker-compose
bash~$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

---

# Setup with docker-compose (cont.)

Once it is open, we can go to [Gitlab Project](https://gitlab.com/silent-mobius/jenkins-compose), clone or download the project to your system, `cd` in to cloned folder and run `docker-compose up -d`
```sh
bash~$ git clone https://gitlab.com/silent-mobius/jenkins-compose.git
bash~$ cd jenkins-compose
bash~$ docker-compose up -d
```
The command will setup all the jenkins images and will run them in `main/worker` configuration.


---

# Jenkins Configuration

Whether you are using `Vagrant` or `Docker`, you will still need to finish the configuration, by accessing the Jenkins UI via web-browser.

- In case of `Vagrant`, you can try accessing the static IP of http://192.168.56.2
  - `[*]` Please verify that is the ip set in vagrant file.
- In case of `Docker`, localhost should be accessible from your browser.
  - `[*]` In case it will not be accessed, try to check that containers are running.


---

# Jenkins Configuration (cont.)

Once you'll be able to access browser, Jenkins will require you to provide the initial login password. It is usually saved under jenkins home folder located at `/var/lib/jenkins/secrets/initialAdminPassword`

<img src="misc/.img/jenkins_login.png" alt="login" style="float:right;width:480px;">


---

# Pop Quiz

If you are making changes at the system level for Jenkins on Ubuntu, you will also need to access your server as the _ _ _ user.

- regular
- service
- local
- root <!-- -->

----

# Pop Quiz

Which Docker command connects to the running container and prints the initial admin password for Jenkins?

- docker exec jenkins cat /var/Jenkins home/secrets/initialAdminPassword <!-- -->
- docker cat /var/jenkins_home/secrets/AdminPassword
- docker exec jenkins /var/jenkins_home/initialAdminPassword
- docker exec jenkins cat /var/secrets/AdminPassword


---

# Jenkins Basics

---
# Jenkins Basics

## Basic UI

Lets take a look on the UI and highlight some of the most used parts.
On the left, the `New Item` menu is the link that you'll probably be using the most. This is where you'll start when you're creating jobs or other resources
Still on the left, Other significant menu that you may encounter, is `Manage Jenkins`,  which will consist Jenkins main configuration and management features.
On the right, mostly login naming, search bar and login/logout button.

<img src='misc/.img/ui.png' alt='ui' style="float:right;width:400px; >


---

# Jenkins Basics

## Manage Jenkins

This menu gives you access to various configurations for your Jenkins server. There's a lot here, and I just want to highlight a couple of them. On a new installation, you may see a notification at the top telling you that building on the controller node can be a security issue. For now, you can safely dismiss this message since we're just getting started on learning how to work with Jenkins. Okay. With that out of the way, I want to focus on just two of the menus here, Global Tool Configuration and Manage Plugins. We'll be using the Global Tool Configuration menu to install tools that all jobs can use. 

<img src='misc/.img/manage.png' alt='manage' style="float:right;width:400px; >



---

# Jobs

<!-- simple jobs -->

## creating job
In order to get started in the Jenkins environment, the first thing that we have to do is actually start creating a job, or a project definition. Ata first we'll do this by using the current `Freestyle project`, to provide simple Jenkins capabilities, and later we'll move to `Pipelines` because that's the most common way of actually thinking about the multiple stages of a continuous integration of our deployment process

## parameters with jobs
## creating multiple steps for a job
## adding scripts as a job step

---

# Builds 

<!-- pipelines-->

## tracking build state
## polling scm for build triggering
## connecting jenkins to SCM
## Webhook build triggering

---

# Agents and Distributed Builds

## adding ssh build agent
## scaling builds with a cloud service
## using docker images for agents
## configure specific agents

---

# Extending Jenkins

## adding plugins
## using share libs

---

# Notifications
## notification of build state
## build state for scm

---

# Testing
## code coverage test and reports
## using test results to stop build

---

# RestAPI
## trigger build via rest-api
## retrieve build status via rest-api

---

# Artifacts

## creating and storing artifacts
# Jenkins Deep Dive


##  About The Course Itself 
We'll learn several topics mainly focused on:

What is CI/CD ?
What is Jenkins ?
Who needs Jenkins ?
How Jenkins works ?
How to manage Jenkins in various environments?


## Who Is This course for ?

Junior/senior sysadmins who have no knowledge of CI/CD with Jenkins.
For junior/senior developers who to implement CI/CD on their private or corporate projects.
For DevOps engineers who wish to implement CI/CD with Jenkins with different groups they work with.
Experienced ops who need refresher in regards CI/CD or Jenkins.


- Intro
- About CI/CD
-  Jenkins Setup
-  Jenkins Fundamentals
    -  Builds
    -  Agents and Distributed Builds
    -  Extending Jenkins
    -  Notifications
    -  Testing
    -  RestAPI
    -  Artifacts
-  Jenkins Pipelines
-  Best Practices

## What course tries to accomplish ?

Eventual goal of the course is to teach its students all in regards to CI/CD <!--specified material --> and to elevate their knowledge to higher level, eventually making them professionals in the field.


---

> `[!]` Note: Please use `build.sh` script to create html version for your use

---
© All Right reserved to Alex M. Schapelle of VaioLabs ltd.
