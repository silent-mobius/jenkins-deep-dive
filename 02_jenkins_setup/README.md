
---

# Jenkins Setup


---

# Setup with Vagrant

In misc folder of this project we can find folder named vagrant, and in it file named `Vagrantfile` which essentially has all the configurations in it  for us to get ready jenkins server. Yet , it usually requires some dependencies:
- Virtualization capable hardware:
  - CPU
  - RAM
  - Storage
- Hypervisor, either [Virtualbox](https://www.virtualbox.org/wiki/Downloads) or [KVM](https://www.linux-kvm.org/page/Main_Page)(only on Linux/Unix) should work out of the box.
- [Vagrant](https://www.vagrantup.com/downloads) itself also is required.

---

# Setup with Vagrant (cont.)

After installing all pre-requisite software, you may change directory to `~/jenkins-deep-dive/misc/set_up/vagrant`, and start the environment as follows:

```sh
bash:~/jenkins-deep-dive/misc/vagrant $ vagrant up 
```
If all goes well, you should have working jenkins environment.

> `[!]` Note: Vagrant will start 3 capable VMs, meaning it will have some-what of a stress on your machine. In case you do not have enough resources such as CPU or ram, it is suggested not to use `Vagrant setup`

---

# Setup with Vagrant (cont.)

In case it fails, Please ask instructor/developer/who ever provided you with this project to help you with subsequent issue.

In other case, in which your host can not comply with CPU overload, RAM stress or Storage in-sufficiency, you are welcome to try and to use `docker-compose setup `


---

# Setup with docker-compose

In cases where using `vagrant` is not an option, we can provide installation of docker and docker-compose.

Lets start with docker installation
```sh
bash:~/$ curl -L get.docker.com | sudo bash 
```
or user `wget` in case `curl` command is

```sh
bash:~/$ wget -O- get.docker.com |sudo bash
```

> `[!]` Note: you need to add your user to docker group and restart your session to gain correct access and use permissions.

---

# Setup with docker-compose (cont.)

Docker Compose relies on *Docker Engine* for any meaningful work. Once it is installed, set and configured (mostly done while installation), we can download  and set `docker-compose`.

```sh
bash~$ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)"\
                 -o /usr/local/bin/docker-compose
bash~$ sudo chmod +x /usr/local/bin/docker-compose
bash~$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

---

# Setup with docker-compose (cont.)

Once it is open, we can go to [Gitlab Project](https://gitlab.com/silent-mobius/jenkins-compose), clone or download the project to your system, `cd` in to cloned folder and run `docker-compose up -d`
```sh
bash~$ git clone https://gitlab.com/silent-mobius/jenkins-compose.git
bash~$ cd jenkins-compose
bash~$ docker-compose up -d
```
The command will setup all the jenkins images and will run them in `main/worker` configuration.


---

# Jenkins Configuration

Whether you are using `Vagrant` or `Docker`, you will still need to finish the configuration, by accessing the Jenkins UI via web-browser.

- In case of `Vagrant`, you can try accessing the static IP of http://192.168.56.2
  - `[*]` Please verify that is the ip set in vagrant file.
- In case of `Docker`, localhost should be accessible from your browser.
  - `[*]` In case it will not be accessed, try to check that containers are running.


---

# Jenkins Configuration (cont.)

Once you'll be able to access browser, Jenkins will require you to provide the initial login password. It is usually saved under jenkins home folder located at `/var/lib/jenkins/secrets/initialAdminPassword`

<img src="misc/.img/jenkins_login.png" alt="login" style="float:right;width:480px;">


---

# Pop Quiz

If you are making changes at the system level for Jenkins on Ubuntu, you will also need to access your server as the _ _ _ user.

- regular
- service
- local
- root <!-- -->

----

# Pop Quiz

Which Docker command connects to the running container and prints the initial admin password for Jenkins?

- docker exec jenkins cat /var/Jenkins home/secrets/initialAdminPassword <!-- -->
- docker cat /var/jenkins_home/secrets/AdminPassword
- docker exec jenkins /var/jenkins_home/initialAdminPassword
- docker exec jenkins cat /var/secrets/AdminPassword

