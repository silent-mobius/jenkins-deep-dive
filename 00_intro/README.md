# Jenkins Deep Dive



.footer: Created By Alex M. Schapelle, VAioLabs.io

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is CI/CD ?
- What is Jenkins ?
- How Jenkins works ?
- What are Jenkins pipelines ?
- How to manage docker in various scenarious ?


### Who Is This course for ?

- Junior/senior developers who wish to automate their development tasks.
- For junior/senior developers/devops/sysops who wish to learn CI/CD Basics with Jenkins as main tool.
- Experienced developers/devops who need refresher.


---

# Course Topics

- Intro
- CI/CD Basics
- Jenkins Environment Setup
- Jenkins Plugins
- Jenkins Pipelines

---

# About Me
<img src="misc/.img/me.jpg" alt="me" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---

# History

In software engineering, CI/CD or CICD is the combined practices of continuous integration (CI) and (more often) continuous delivery or (less often) continuous deployment (CD).

CI/CD bridges the gaps between development and operation activities and teams by enforcing automation in building, testing and deployment of applications. CI/CD services compile the incremental code changes made by developers, then link and package them into software deliverables. 

Automated tests verify the software functionality, and automated deployment services deliver them to end users. 

---

# History (cont.)

The aim is to increase early defect discovery, increase productivity, and provide faster release cycles. The process contrasts with traditional methods where a collection of software updates were integrated into one large batch before deploying the newer version. 

Modern-day DevOps practices involve continuous development, continuous testing, continuous integration, continuous deployment and continuous monitoring of software applications throughout its development life cycle. The CI/CD practice, or CI/CD pipeline, forms the backbone of modern day DevOps operations. 

---
# History (cont.)

## Jenkins

Kohsuke Kawaguchi worked at Sun Microsystems on numerous projects for the Java, XML and Solaris ecosystems, notably as the primary developer for Hudson and for Multi Schema Validator. Hudson was created in summer of 2004 and first released in February 2005.

During November 2010, after the acquisition of Sun Microsystems by Oracle, an issue arose in the Hudson community with respect to the infrastructure used, which grew to encompass questions over the stewardship and control by Oracle. Negotiations between the principal project contributors and Oracle took place, and although there were many areas of agreement a key sticking point was the trademarked name "Hudson," after Oracle claimed the right to the name and applied for a trademark in December 2010. As a result, on January 11, 2011, a call for votes was made to change the project name from "Hudson" to "Jenkins." The proposal was overwhelmingly approved by community vote on January 29, 2011, creating the Jenkins project.

---
# History (cont.)

## Jenkins

On February 1, 2011, Oracle said that they intended to continue development of Hudson, and considered Jenkins a fork rather than a rename. Jenkins and Hudson therefore continued as two independent projects,each claiming the other is the fork. As of June 2019, the Jenkins organization on GitHub had 667 project members and around 2,200 public repositories, compared with Hudson's 28 project members and 20 public repositories with the last update in 2016.

On April 20, 2016 version 2 was released with the Pipeline plugin enabled by default. The plugin allows for writing build instructions using a domain specific language based on Apache Groovy.

---

# Why Jenkins

- Easy of use
- 1200+ plugins
- Reporting
- Distributed builds

---

# Jenkins in nutshell

- Originally written by Kohsuke Kawaguchi (as Hudson)
- Open-source CI Server
- Released in 2008
- Renamed to Jenkins in 2011
- 1800+ plugins available on official site
- http://jenkins.io

