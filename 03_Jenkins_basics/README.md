
---

# Jenkins Basics

---
# Jenkins Basics

## Basic UI

Lets take a look on the UI and highlight some of the most used parts.
On the left, the `New Item` menu is the link that you'll probably be using the most. This is where you'll start when you're creating jobs or other resources
Still on the left, Other significant menu that you may encounter, is `Manage Jenkins`,  which will consist Jenkins main configuration and management features.
On the right, mostly login naming, search bar and login/logout button.

<img src='misc/.img/ui.png' alt='ui' style="float:right;width:400px; >


---

# Jenkins Basics

## Manage Jenkins

This menu gives you access to various configurations for your Jenkins server. There's a lot here, and I just want to highlight a couple of them. On a new installation, you may see a notification at the top telling you that building on the controller node can be a security issue. For now, you can safely dismiss this message since we're just getting started on learning how to work with Jenkins. Okay. With that out of the way, I want to focus on just two of the menus here, Global Tool Configuration and Manage Plugins. We'll be using the Global Tool Configuration menu to install tools that all jobs can use. 

<img src='misc/.img/manage.png' alt='manage' style="float:right;width:400px; >



---

# Jobs

<!-- simple jobs -->

## creating job
In order to get started in the Jenkins environment, the first thing that we have to do is actually start creating a job, or a project definition. Ata first we'll do this by using the current `Freestyle project`, to provide simple Jenkins capabilities, and later we'll move to `Pipelines` because that's the most common way of actually thinking about the multiple stages of a continuous integration of our deployment process

## parameters with jobs
## creating multiple steps for a job
## adding scripts as a job step

---

# Builds 

<!-- pipelines-->

## tracking build state
## polling scm for build triggering
## connecting jenkins to SCM
## Webhook build triggering

---

# Agents and Distributed Builds

## adding ssh build agent
## scaling builds with a cloud service
## using docker images for agents
## configure specific agents

---

# Extending Jenkins

## adding plugins
## using share libs

---

# Notifications
## notification of build state
## build state for scm

---

# Testing
## code coverage test and reports
## using test results to stop build

---

# RestAPI
## trigger build via rest-api
## retrieve build status via rest-api

---

# Artifacts

## creating and storing artifacts
